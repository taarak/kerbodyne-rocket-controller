package com.kerbodyne.kerbolang.comms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.ardulink.core.Link;
import org.ardulink.core.Pin;
import org.ardulink.core.convenience.Links;
import org.ardulink.core.events.AnalogPinValueChangedEvent;
import org.ardulink.core.events.DigitalPinValueChangedEvent;
import org.ardulink.core.events.EventListener;

public class ArdulinkCommunotron implements Communotron {
    private Link link;
    private Map<Integer, Boolean> digitalPinStates;
    private Map<Integer, Integer> analogPinStates;
    private List<MessageReceivedListener> messageReceivedListeners;

    public ArdulinkCommunotron() {
        link = Links.getDefault();
        digitalPinStates = new ConcurrentHashMap<>();
        analogPinStates = new ConcurrentHashMap<>();
        messageReceivedListeners = new ArrayList<>();

        try {
            link.addListener(new EventListener() {
                @Override
                public void stateChanged(AnalogPinValueChangedEvent event) {
                    analogPinStates.put(event.getPin().pinNum(), event.getValue());
                }

                @Override
                public void stateChanged(DigitalPinValueChangedEvent event) {
                    digitalPinStates.put(event.getPin().pinNum(), event.getValue());
                }
            });
            link.addCustomListener(ce -> messageReceivedListeners.forEach(l -> l.messageReceived(ce.getMessage())));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPinState(int pin, boolean state) {
        try {
            link.switchDigitalPin(Pin.digitalPin(pin), state);
        } catch (IOException e) {
            System.err.println("Não foi possível alterar o estado do pino digital " + pin + "!");
            e.printStackTrace();
            System.exit(47); // 4 é como um 'A' e 7 é como um 'L' invertido
        }
    }

    @Override
    public void setPinState(int pin, int intensity) {
        try {
            link.switchAnalogPin(Pin.analogPin(pin), intensity);
        } catch (IOException e) {
            System.err.println("Não foi possível alterar o estado do pino analógico " + pin + "!");
            e.printStackTrace();
            System.exit(47); // 4 é como um 'A' e 7 é como um 'L' invertido
        }
    }

    @Override
    public void sendMessage(String message) {
        try {
            link.sendCustomMessage(message);
        } catch (IOException e) {
            System.err.println("Não foi possível enviar mensagem para a placa Arduino!");
            e.printStackTrace();
            System.exit(47); // 4 é como um 'A' e 7 é como um 'L' invertido
        }
    }

    @Override
    public boolean readDigitalPin(int pin) {
        return digitalPinStates.get(pin);
    }

    @Override
    public int readAnalogPin(int pin) {
        return analogPinStates.get(pin);
    }

    @Override
    public void addMessageReceivenListener(MessageReceivedListener messageReceivedListener) {
        messageReceivedListeners.add(messageReceivedListener);
    }

    @Override
    public void close() throws IOException {
        link.close();
    }

    public Link getLink() {
        return link;
    }
}
