package com.kerbodyne.kerbolang.comms;

import java.io.Closeable;

public interface Communotron extends Closeable {
    void setPinState(int pin, boolean state);

    void setPinState(int pin, int intensity);

    void sendMessage(String message);

    boolean readDigitalPin(int pin);

    int readAnalogPin(int pin);

    void addMessageReceivenListener(MessageReceivedListener messageReceivedListener);
}
