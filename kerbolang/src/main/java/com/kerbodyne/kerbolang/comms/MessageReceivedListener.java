package com.kerbodyne.kerbolang.comms;

@FunctionalInterface
public interface MessageReceivedListener {
    void messageReceived(String message);
}
