package com.kerbodyne.kerbolang;

public class ConstantAssignmentError extends Error {
    public ConstantAssignmentError(String message) {
        super(message);
    }
}
