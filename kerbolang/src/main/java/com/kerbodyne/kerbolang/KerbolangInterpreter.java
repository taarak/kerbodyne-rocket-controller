package com.kerbodyne.kerbolang;

import com.kerbodyne.kerbolang.comms.Communotron;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.structure.Keyword;
import com.kerbodyne.kerbolang.structure.Variable;
import com.kerbodyne.kerbolang.structure.keywords.ExecuteWhen;
import com.kerbodyne.kerbolang.tokenizer.Lexer;
import com.kerbodyne.kerbolang.tokenizer.Token;
import com.kerbodyne.kerbolang.tokenizer.TokenType;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import org.reflections.Reflections;

public class KerbolangInterpreter {
    private static Communotron communotron;
    private static PrintStream printStream;

    private static boolean trace;

    private static boolean canceled;

    static {
        printStream = System.out;
    }

    public static PrintStream getPrintStream() {
        return printStream;
    }

    public static void setPrintStream(PrintStream printStream) {
        KerbolangInterpreter.printStream = printStream;
    }

    public static boolean isTrace() {
        return trace;
    }

    public static void setTrace(boolean trace) {
        KerbolangInterpreter.trace = trace;
    }

    public static Communotron getCommunotron() {
        return communotron;
    }

    public static void setCommunotron(Communotron communotron) {
        KerbolangInterpreter.communotron = communotron;
    }

    public static void startup() {
        Reflections reflections = new Reflections("com.kerbodyne.kerbolang");
        List<Class<?>> keywordClasses = new ArrayList<>(reflections.getTypesAnnotatedWith(KerbolangElement.class));
        List<Keyword> keywords = new ArrayList<>();
        keywordClasses.forEach(c -> {
            try {
                keywords.add((Keyword) c.getDeclaredConstructor().newInstance());
            } catch (InstantiationException | NoSuchMethodException | IllegalAccessException |
                    InvocationTargetException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Erro construindo elementos Kerbolang!",
                        "Erro Interno Kerbolang!", JOptionPane.ERROR_MESSAGE);
            }
        });
        Keyword.setKeywords(keywords);
    }

    public static void cancel() {
        canceled = true;
    }

    public static void clearVariables() {
        Variable.clear();
    }

    public void interpret(String kerbolangCode) {
        interpret(Lexer.lex(kerbolangCode), 0);
    }

    public void interpret(List<Token> tokens, int currentLine) {
        try {
            interpretRaw(tokens, currentLine);
        } catch (Throwable e) {
            e.printStackTrace(printStream);
            if (printStream != System.out) {
                e.printStackTrace();
            }
        }
    }

    private void interpretRaw(List<Token> tokens, int currentLine) {
        canceled = false; // Caso o interpretador seja cancelado antes de começar a interpretar código

        if (tokens.get(tokens.size() - 1).getTokenType() != TokenType.EOS) {
            tokens.add(new Token(TokenType.EOS, "\n"));
        }

        Iterator<Token> tokenIterator = tokens.iterator();
        List<Token> currentBlock = new ArrayList<>();
        int lineNumber = currentLine + 1;
        boolean insideBlock = false;
        while (tokenIterator.hasNext()) {
            if (canceled) {
                canceled = false;
                return;
            }

            Token token = tokenIterator.next();
            if (token.getTokenType() == TokenType.ENTAOKEYWORD) {
                insideBlock = true;
            }

            if ((token.getTokenType() != TokenType.EOS && !insideBlock) ||
                    (token.getTokenType() != TokenType.FIMKEYWORD && insideBlock)) {
                currentBlock.add(token);
            } else if (currentBlock.size() == 0) {
                lineNumber++;
            } else {
                insideBlock = false;
                boolean syntaxError = true;

                for (Keyword keyword : Keyword.getKeywords()) {
                    if (keyword.matches(currentBlock)) {
                        try {
                            keyword.run(currentBlock, lineNumber);
                        } catch (SyntaxError e) { // Caso um bloco seja executado, o erro vai se propagar
                            // O erro lançado vai se propagar até o método de origem, que vai imprimi-la.
                            throw new SyntaxError(e.getMessage(), e.getLine() + lineNumber);
                        }
                        syntaxError = false;
                        break;
                    }
                }

                iterateWhens();

                if (syntaxError) {
                    throw new SyntaxError("Sintaxe inválida na linha {l}!", lineNumber);
                }
                lineNumber++;
                currentBlock.clear();
            }
        }
    }

    private void iterateWhens() {
        ExecuteWhen.getScheduledStatements().forEach(s -> {
            if (s.isTrue()) { // Para aumentar a performance em loops, deixei a cargo de quem executa
                // verificar se o statement é verdadeiro
                ExecuteWhen.getScheduledStatements().remove(s);
                s.run(null, -1);
            }
        });
    }
}
