package com.kerbodyne.kerbolang.structure.functions;

import com.kerbodyne.kerbolang.KerbolangInterpreter;
import com.kerbodyne.kerbolang.comms.Communotron;
import com.kerbodyne.kerbolang.structure.Function;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.tokenizer.Token;
import java.util.List;

@KerbolangElement
public class ResetArduino extends Function {
    public ResetArduino() {
        super("reiniciarArduino");
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        Communotron communotron = KerbolangInterpreter.getCommunotron();
        communotron.sendMessage("reset");
    }
}
