package com.kerbodyne.kerbolang.structure.keywords;

import com.kerbodyne.kerbolang.structure.ConditionalStatement;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.structure.Keyword;
import com.kerbodyne.kerbolang.tokenizer.Token;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.kerbodyne.kerbolang.tokenizer.TokenType.CLOSEPARENTHESIS;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.ENTAOKEYWORD;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.OPENPARENTHESIS;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.QUANDOKEYWORD;

@KerbolangElement
public class ExecuteWhen extends Keyword {
    private volatile static CopyOnWriteArrayList<ConditionalStatement> statements;

    static {
        statements = new CopyOnWriteArrayList<>();
    }

    public static CopyOnWriteArrayList<ConditionalStatement> getScheduledStatements() {
        return statements;
    }

    @Override
    public boolean matches(List<Token> tokens) {
        if (tokens.size() < 6) {
            return false;
        }

        if (typeDiffers(tokens.get(0), QUANDOKEYWORD) ||
                !hasInRange(tokens, ENTAOKEYWORD, 4, tokens.size())) {
            return false;
        }

        return isValidExpression(getTokensBetween(tokens, OPENPARENTHESIS, CLOSEPARENTHESIS));
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        int entaoIndex = getIndex(tokens, ENTAOKEYWORD);
        ConditionalStatement statement = new ConditionalStatement(getTokensBetween(tokens, OPENPARENTHESIS,
                CLOSEPARENTHESIS), new CopyOnWriteArrayList<>(tokens.subList(entaoIndex + 1, tokens.size())));
        statements.add(statement);
    }
}
