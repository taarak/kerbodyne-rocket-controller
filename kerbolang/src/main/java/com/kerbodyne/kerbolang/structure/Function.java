package com.kerbodyne.kerbolang.structure;

import com.kerbodyne.kerbolang.tokenizer.Token;
import com.kerbodyne.kerbolang.tokenizer.TokenType;
import java.util.ArrayList;
import java.util.List;

import static com.kerbodyne.kerbolang.tokenizer.TokenType.CLOSEPARENTHESIS;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.COMMA;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.IDENTIFIER;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.OPENPARENTHESIS;

public abstract class Function extends Keyword {
    private String name;

    public Function(String name) {
        this.name = name;
    }

    @Override
    public boolean matches(List<Token> tokens) {
        if (tokens.get(0).getTokenType() != IDENTIFIER || !tokens.get(0).getToken().equals(name)) {
            return false;
        }

        if (tokens.get(1).getTokenType() != OPENPARENTHESIS ||
                tokens.get(tokens.size() - 1).getTokenType() != CLOSEPARENTHESIS) {
            return false;
        }

        List<Token> parenthesisTokens = getTokensBetween(tokens, OPENPARENTHESIS, CLOSEPARENTHESIS);
        List<List<Token>> expressions = divideList(parenthesisTokens, COMMA);
        for (List<Token> expression : expressions) {
            if (!isValidExpression(expression)) {
                return false;
            }
        }

        return true;
    }

    protected List<Object> getParenthesisValues(List<Token> parenthesisTokens) {
        List<Object> values = new ArrayList<>();
        List<List<Token>> expressions = divideList(parenthesisTokens, COMMA);
        for (List<Token> expression : expressions) {
            values.add(evaluate(expression));
        }
        return values;
    }

    private List<List<Token>> divideList(List<Token> initial, TokenType type) {
        List<List<Token>> result = new ArrayList<>();
        List<Token> currentList = new ArrayList<>();
        for (Token token : initial) {
            if (token.getTokenType() != type) {
                currentList.add(token);
            } else {
                if (currentList.size() != 0) {
                    result.add(currentList);
                    currentList = new ArrayList<>();
                }
            }
        }
        result.add(currentList); // O valor depois do tipo especificado nunca será adicionado pelo loop
        return result;
    }
}
