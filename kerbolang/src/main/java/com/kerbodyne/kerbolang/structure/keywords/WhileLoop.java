package com.kerbodyne.kerbolang.structure.keywords;

import com.kerbodyne.kerbolang.KerbolangInterpreter;
import com.kerbodyne.kerbolang.structure.ConditionalStatement;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.structure.Keyword;
import com.kerbodyne.kerbolang.tokenizer.Token;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.kerbodyne.kerbolang.tokenizer.TokenType.CLOSEPARENTHESIS;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.ENQUANTOKEYWORD;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.ENTAOKEYWORD;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.OPENPARENTHESIS;

@KerbolangElement
public class WhileLoop extends Keyword {
    @Override
    public boolean matches(List<Token> tokens) {
        if (tokens.size() < 7) { // enquanto ( cond ) entao body fim
            return false;
        }

        return !typeDiffers(tokens.get(0), ENQUANTOKEYWORD) && !typeDiffers(tokens.get(1), OPENPARENTHESIS);

        // TODO: Implementar verificação da parte "condicao) faca codigo fim"

    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        if (KerbolangInterpreter.isTrace()) {
            KerbolangInterpreter.getPrintStream().println("Entrando em loop 'enquanto'!");
        }

        // Não uso um range pois isso reduz a performance e ele vem sem o token "fim" no final
        // Por algum motivo, quando o keyword "faca" é adicionado o loop é executado para sempre, mas não é executado
        // de verdade. Se adicionar um +1 no index, ele adicionado somente o \n depois do token, e se adicionar um +2,
        // ele não adiciona nem isso.
        ConditionalStatement statement = new ConditionalStatement(getTokensBetween(tokens, OPENPARENTHESIS,
                CLOSEPARENTHESIS), new CopyOnWriteArrayList<>(
                tokens.subList(getIndex(tokens, ENTAOKEYWORD) + 2, tokens.size())));
        while (statement.isTrue()) {
            statement.run(null, lineNumber);
        }

        if (KerbolangInterpreter.isTrace()) {
            KerbolangInterpreter.getPrintStream().println("Saindo de loop 'enquanto'!");
        }
    }
}
