package com.kerbodyne.kerbolang.structure.functions;

import com.kerbodyne.kerbolang.KerbolangInterpreter;
import com.kerbodyne.kerbolang.SyntaxError;
import com.kerbodyne.kerbolang.comms.Communotron;
import com.kerbodyne.kerbolang.structure.Function;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.tokenizer.Token;
import com.kerbodyne.kerbolang.tokenizer.TokenType;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@KerbolangElement
public class Trigger extends Function {
    private static final ScheduledExecutorService executor;

    static {
        executor = Executors.newSingleThreadScheduledExecutor();
    }

    public Trigger() {
        super("acione");
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        List<Object> args = getParenthesisValues(getTokensBetween(tokens, TokenType.OPENPARENTHESIS,
                TokenType.CLOSEPARENTHESIS));
        int pin = ((Double) args.get(0)).intValue();
        Object state = args.get(1); // Pode ser um Double ou um Boolean
        setState(pin, state);

        if (args.size() > 2) {
            int time = ((Double) args.get(2)).intValue();
            Runnable action;
            action = () -> setState(pin, (state instanceof Boolean ? false : 0));
            executor.schedule(action, time, TimeUnit.MILLISECONDS);
        }
    }

    private void setState(int pin, Object state) {
        Communotron communotron = KerbolangInterpreter.getCommunotron();
        if (state instanceof Boolean) {
            communotron.setPinState(pin, (Boolean) state);
        } else if (state instanceof Double) {
            communotron.setPinState(pin, ((Double) state).intValue());
        } else {
            throw new SyntaxError("Tipo inválido ao tentar acionar um pino do Arduino!");
        }
    }
}
