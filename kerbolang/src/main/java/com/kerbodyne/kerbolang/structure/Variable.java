package com.kerbodyne.kerbolang.structure;

import com.kerbodyne.kerbolang.ConstantAssignmentError;
import com.kerbodyne.kerbolang.KerbolangInterpreter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Variable {
    public static volatile Map<String, Variable> stack;

    static {
        stack = new ConcurrentHashMap<>();
        clear();
    }

    private String identifier;
    private Object value;
    private boolean constant;

    public Variable(String identifier, Object value, boolean constant) {
        this.identifier = identifier;
        this.value = value;
        this.constant = constant;
        stack.put(identifier, this);
        if (KerbolangInterpreter.isTrace()) {
            KerbolangInterpreter.getPrintStream()
                    .println("Criada " + (constant ? "constante" : "variável") + " com identificador '" + identifier +
                            (value != null ? "' e valor '" + value + "'!" : "'!"));
        }
    }

    public String getIdentifier() {
        return identifier;
    }

    /* Variáveis não podem trocar de nome */

    public Object getValue() {
        return value;
    }

    public static Variable getFromStack(String identifier) {
        return stack.get(identifier);
    }

    public boolean isConstant() {
        return constant;
    }

    /* Variáveis constantes não podem deixar de ser constantes */

    public void setValue(Object value) {
        if (!constant) {
            if (this.value != null && !this.value.getClass().equals(value.getClass())) {
                KerbolangInterpreter.getPrintStream().println("Variável '" + identifier + "' mudando de tipo!");
            }
            this.value = value;
        } else {
            throw new ConstantAssignmentError("Não é possível modificar o valor de uma constante!");
        }
    }

    public static void clear() {
        stack.clear();
        boolean wasTrace = KerbolangInterpreter.isTrace();
        KerbolangInterpreter.setTrace(false);
        new Variable("pi", Math.PI, true);
        new Variable("e", Math.E, true);
        new Variable("g", 9.807d, true);
        KerbolangInterpreter.setTrace(wasTrace);
    }
}
