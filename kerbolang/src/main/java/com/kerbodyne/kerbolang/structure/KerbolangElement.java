package com.kerbodyne.kerbolang.structure;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
// Com retention CLASS ou RUNTIME funciona do mesmo jeito, mas RUNTIME parece ser melhor
@Target(ElementType.TYPE)
public @interface KerbolangElement {
}
