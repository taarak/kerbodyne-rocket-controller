package com.kerbodyne.kerbolang.structure;

import com.kerbodyne.kerbolang.KerbolangInterpreter;
import com.kerbodyne.kerbolang.SyntaxError;
import com.kerbodyne.kerbolang.tokenizer.Token;
import java.util.ArrayList;
import java.util.List;

public class ConditionalStatement extends Keyword {
    private List<Token> code;
    private List<Token> condition;

    public ConditionalStatement(List<Token> condition, List<Token> code) {
        this.code = new ArrayList<>(code);
        this.condition = new ArrayList<>(condition);
    }

    @Override
    public boolean matches(List<Token> tokens) {
        return isValidExpression(condition); // O código será analisado depois
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        new KerbolangInterpreter().interpret(code, lineNumber);
    }

    public boolean isTrue() {
        try {
            return ((Boolean) evaluate(condition));
        } catch (ClassCastException e) {
            throw new SyntaxError("Expressão em bloco condicional inválida!", 0);
        }
    }
}
