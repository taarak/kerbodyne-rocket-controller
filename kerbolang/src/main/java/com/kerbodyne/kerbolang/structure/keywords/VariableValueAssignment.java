package com.kerbodyne.kerbolang.structure.keywords;

import com.kerbodyne.kerbolang.SyntaxError;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.structure.Keyword;
import com.kerbodyne.kerbolang.structure.Variable;
import com.kerbodyne.kerbolang.tokenizer.Token;
import com.kerbodyne.kerbolang.tokenizer.TokenType;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.kerbodyne.kerbolang.tokenizer.TokenType.ASSIGNMENTOPERATOR;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.BOOLEANLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.HEXLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.IDENTIFIER;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.LOGICALOPERATOR;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.NUMBERLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.RELATIONALOPERATOR;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.STRINGLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.UNARYASSIGNMENTOPERATOR;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.UNARYOPERATOR;

@KerbolangElement
public class VariableValueAssignment extends Keyword {
    @Override
    public boolean matches(List<Token> tokens) {
        if (tokens.size() < 3) {
            return false;
        }

        if (typeDiffers(tokens.get(0), IDENTIFIER) || (typeDiffers(tokens.get(1), TokenType.ASSIGNMENTOPERATOR)
                && typeDiffers(tokens.get(1), UNARYASSIGNMENTOPERATOR))) {
            return false;
        }
        for (Token token : new CopyOnWriteArrayList<>(tokens.subList(2, tokens.size()))) {
            if (typeDiffers(token, STRINGLITERAL) && typeDiffers(token, HEXLITERAL) &&
                    typeDiffers(token, NUMBERLITERAL) && typeDiffers(token, BOOLEANLITERAL) &&
                    typeDiffers(token, IDENTIFIER) && typeDiffers(token, UNARYOPERATOR) &&
                    typeDiffers(token, RELATIONALOPERATOR) && typeDiffers(token, LOGICALOPERATOR)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        Object value = evaluate(new CopyOnWriteArrayList<>(tokens.subList(2, tokens.size())));

        String identifier = tokens.get(0).getToken();
        Variable var = Variable.getFromStack(identifier);
        if (var == null) {
            throw new SyntaxError("Variável " + identifier + " não existe! Crie-a com 'def " + identifier + "'!");
        }

        Token assignmentToken = tokens.get(1);
        if (assignmentToken.getTokenType() == ASSIGNMENTOPERATOR) {
            var.setValue(value);
        } else {
            switch (assignmentToken.getToken().charAt(0)) {
                case '+':
                    var.setValue(((Double) var.getValue()) + ((Double) value));
                    break;
                case '/':
                    var.setValue(((Double) var.getValue()) / ((Double) value));
                    break;
                case '*':
                    var.setValue(((Double) var.getValue()) * ((Double) value));
                    break;
                case '-':
                    var.setValue(((Double) var.getValue()) - ((Double) value));
                    break;
            }
        }
    }
}
