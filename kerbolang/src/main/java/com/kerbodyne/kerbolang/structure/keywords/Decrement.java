package com.kerbodyne.kerbolang.structure.keywords;

import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.structure.Keyword;
import com.kerbodyne.kerbolang.structure.Variable;
import com.kerbodyne.kerbolang.tokenizer.Token;
import com.kerbodyne.kerbolang.tokenizer.TokenType;
import java.util.List;

@KerbolangElement
public class Decrement extends Keyword {
    @Override
    public boolean matches(List<Token> tokens) {
        return tokens.get(0).getTokenType() == TokenType.IDENTIFIER && tokens.get(1).getTokenType() == TokenType.DECREMENT;
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        Variable var = Variable.getFromStack(tokens.get(0).getToken());
        var.setValue(((double) var.getValue()) - 1); // Por algum motivo '--((double) var.getValue())' também não funciona
    }
}
