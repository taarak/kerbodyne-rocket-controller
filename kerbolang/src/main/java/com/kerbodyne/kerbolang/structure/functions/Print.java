package com.kerbodyne.kerbolang.structure.functions;

import com.kerbodyne.kerbolang.KerbolangInterpreter;
import com.kerbodyne.kerbolang.structure.Function;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.tokenizer.Token;
import com.kerbodyne.kerbolang.tokenizer.TokenType;
import java.util.List;

@KerbolangElement
public class Print extends Function {
    public Print() {
        super("escreva");
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        StringBuilder resultString = new StringBuilder();
        for (Object obj : getParenthesisValues(getTokensBetween(tokens, TokenType.OPENPARENTHESIS,
                TokenType.CLOSEPARENTHESIS))) {
            resultString.append(obj);
        }

        KerbolangInterpreter.getPrintStream().print(resultString.toString());
        KerbolangInterpreter.getPrintStream().flush();
    }
}
