package com.kerbodyne.kerbolang.structure.keywords;

import com.kerbodyne.kerbolang.structure.ConditionalStatement;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.structure.Keyword;
import com.kerbodyne.kerbolang.tokenizer.Token;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.kerbodyne.kerbolang.tokenizer.TokenType.CLOSEPARENTHESIS;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.ENTAOKEYWORD;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.OPENPARENTHESIS;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.SEKEYWORD;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.SENAOKEYWORD;

@KerbolangElement
public class ExecuteIf extends Keyword {
    @Override
    public boolean matches(List<Token> tokens) {
        if (tokens.size() < 6) {
            return false;
        }

        if (typeDiffers(tokens.get(0), SEKEYWORD) ||
                !hasInRange(tokens, ENTAOKEYWORD, 4, tokens.size())) {
            return false;
        }

        return isValidExpression(getTokensBetween(tokens, OPENPARENTHESIS, CLOSEPARENTHESIS));
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        int entaoIndex = getIndex(tokens, ENTAOKEYWORD);
        int runIfLastIndex = getIndex(tokens, SENAOKEYWORD);
        boolean hasElse = true;
        if (runIfLastIndex == -1) {
            runIfLastIndex = tokens.size() - 1;
            hasElse = false;
        }
        ConditionalStatement runIf = new ConditionalStatement(getTokensBetween(tokens, OPENPARENTHESIS,
                CLOSEPARENTHESIS), new CopyOnWriteArrayList<>(tokens.subList(entaoIndex + 1, runIfLastIndex)));
        ConditionalStatement runElse = null;
        if (hasElse) {
            runElse = new ConditionalStatement(new ArrayList<>(), new CopyOnWriteArrayList<>(
                    tokens.subList(runIfLastIndex + 2, tokens.size() - 1)));
        }

        if (runIf.isTrue()) {
            runIf.run(null, -1);
        } else if (runElse != null) {
            runElse.run(null, -1);
        }
    }
}
