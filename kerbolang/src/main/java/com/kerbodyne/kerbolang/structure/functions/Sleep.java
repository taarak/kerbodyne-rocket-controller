package com.kerbodyne.kerbolang.structure.functions;

import com.kerbodyne.kerbolang.KerbolangInternalError;
import com.kerbodyne.kerbolang.SyntaxError;
import com.kerbodyne.kerbolang.structure.Function;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.tokenizer.Token;
import com.kerbodyne.kerbolang.tokenizer.TokenType;
import java.util.List;

@KerbolangElement
public class Sleep extends Function {
    public Sleep() {
        super("espere");
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        List<Object> parenthesisValues = getParenthesisValues(getTokensBetween(tokens, TokenType.OPENPARENTHESIS,
                TokenType.CLOSEPARENTHESIS));
        if (parenthesisValues.size() > 2) {
            throw new SyntaxError("Mais argumentos do que o necessário!");
        }

        double ms = (Double) parenthesisValues.get(0);
        double ns = 0;
        if (parenthesisValues.size() > 1) {
            ns = (Double) parenthesisValues.get(1);
        }

        try {
            Thread.sleep((int) ms, (int) ns);
        } catch (InterruptedException e) {
            throw new KerbolangInternalError("Não foi possível executar a função 'espere'!", e);
        }
    }
}
