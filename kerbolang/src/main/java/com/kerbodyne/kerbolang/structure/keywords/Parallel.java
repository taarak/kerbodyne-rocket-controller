package com.kerbodyne.kerbolang.structure.keywords;

import com.kerbodyne.kerbolang.KerbolangInterpreter;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.structure.Keyword;
import com.kerbodyne.kerbolang.tokenizer.Token;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.kerbodyne.kerbolang.tokenizer.TokenType.ENTAOKEYWORD;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.FIMKEYWORD;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.PARALELOKEYWORD;

@KerbolangElement
public class Parallel extends Keyword {
    @Override
    public boolean matches(List<Token> tokens) {
        if (tokens.size() < 4) {
            return false;
        }

        return !typeDiffers(tokens.get(0), PARALELOKEYWORD) || !typeDiffers(tokens.get(1), ENTAOKEYWORD) ||
                !typeDiffers(tokens.get(tokens.size() - 1), FIMKEYWORD);

    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        new Thread(() -> new KerbolangInterpreter()
                .interpret(new CopyOnWriteArrayList<>(tokens.subList(2, tokens.size() - 1)), lineNumber)).start();
    }
}
