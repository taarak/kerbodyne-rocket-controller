package com.kerbodyne.kerbolang.structure.keywords;

import com.kerbodyne.kerbolang.SyntaxError;
import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.structure.Keyword;
import com.kerbodyne.kerbolang.structure.Variable;
import com.kerbodyne.kerbolang.tokenizer.Token;
import com.kerbodyne.kerbolang.tokenizer.TokenType;
import java.util.List;

@KerbolangElement
public class Increment extends Keyword {
    @Override
    public boolean matches(List<Token> tokens) {
        return tokens.get(0).getTokenType() == TokenType.IDENTIFIER && tokens.get(1).getTokenType() == TokenType.INCREMENT;
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        String identifier = tokens.get(0).getToken();
        Variable var = Variable.getFromStack(identifier);
        if (var != null) {
            var.setValue(((double) var.getValue()) + 1); // Por algum motivo '++((double) var.getValue())' não funciona
        } else {
            throw new SyntaxError("Variável " + identifier + " não existe! Crie-a com 'def " + identifier + "'!");
        }
    }
}
