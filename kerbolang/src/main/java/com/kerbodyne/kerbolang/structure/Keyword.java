package com.kerbodyne.kerbolang.structure;

import com.kerbodyne.kerbolang.SyntaxError;
import com.kerbodyne.kerbolang.tokenizer.Token;
import com.kerbodyne.kerbolang.tokenizer.TokenType;
import java.util.ArrayList;
import java.util.List;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import static com.kerbodyne.kerbolang.tokenizer.TokenType.BOOLEANLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.CLOSEPARENTHESIS;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.HEXLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.IDENTIFIER;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.LOGICALOPERATOR;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.NUMBERLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.OPENPARENTHESIS;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.RELATIONALOPERATOR;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.STRINGLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.UNARYOPERATOR;

public abstract class Keyword {
    private static ScriptEngine scriptEngine;
    private static List<Keyword> keywords;

    static {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        scriptEngine = scriptEngineManager.getEngineByName("graal.js");
    }

    public abstract boolean matches(List<Token> tokens);

    public abstract void run(List<Token> tokens, int lineNumber);

    public static List<Keyword> getKeywords() {
        return keywords;
    }

    public static void setKeywords(List<Keyword> keywords) {
        Keyword.keywords = keywords;
    }

    protected final boolean typeDiffers(Token token, TokenType tokenType) { // Parece extremamente desnecessário, mas até
        // que é bem útil, considerando que ele é usado muitas vezes, e não ter que digitar token.getTokenType() é
        // muito bom
        return token.getTokenType() != tokenType;
    }

    protected final List<Token> getTokensBetween(List<Token> tokens, TokenType start, TokenType end) {
        List<Token> result = new ArrayList<>();

        boolean collecting = false;
        int opened = -1;
        for (Token token : tokens) {
            if (token.getTokenType() == start) {
                collecting = true;
                opened++;
                if (opened == 0) {
                    opened++;
                    continue;
                }
            } else if (token.getTokenType() == end) {
                collecting = false;
                opened--;
                if (opened == 0) {
                    break;
                } else {
                    result.add(token);
                }
            }

            if (collecting) {
                result.add(token);
            }
        }

        if (opened != 0) {
            throw new SyntaxError("Nem todos os blocos abertos foram fechados!", 0);
        }

        return result;
    }

    private Object getValue(Token token) {
        Object value;
        if (token.getTokenType() == IDENTIFIER) {
            String identifier = token.getToken();
            Variable var = Variable.getFromStack(identifier);
            if (var == null) {
                throw new SyntaxError("Variável '" + identifier + "' não existe! Crie-a com 'def " + identifier + "'!");
            }
            value = var.getValue();
        } else if (token.getTokenType() == STRINGLITERAL) {
            // Remove as aspas duplas no começo e no fim da String e transforma o texto "\n" no line-break "\n".
            value = token.getToken()
                    .substring(1, token.getToken().length() - 1)
                    .replaceAll("\\\\n", "\n");
        } else if (token.getTokenType() == HEXLITERAL) {
            value = (double) Long.parseLong(token.getToken().substring(2), 16);
        } else if (token.getTokenType() == NUMBERLITERAL) {
            value = Double.valueOf(token.getToken());
        } else if (token.getTokenType() == BOOLEANLITERAL) {
            value = token.getToken().equals("ligado"); // O valor só pode ser 'ligado' ou 'desligado', então se
            // não for 'ligado', significa que só pode ser 'desligado'
        } else {
            throw new SyntaxError("Sintaxe inválida! Não é possível reconhecer o valor!");
        }
        return value;
    }

    protected final Object evaluate(List<Token> tokens) {
        if (tokens.size() == 1) {
            return getValue(tokens.get(0));
        }

        if (!isValidExpression(tokens)) {
            throw new SyntaxError("Expressão inválida!");
        }

        boolean stringConcat = false;
        for (Token token : tokens) {
            if (token.getTokenType() == STRINGLITERAL) {
                stringConcat = true;
            }
        }

        StringBuilder sb = new StringBuilder();
        for (Token t : tokens) {
            if (!stringConcat) {
                Object obj;
                TokenType tt = t.getTokenType();
                if (tt != UNARYOPERATOR && tt != RELATIONALOPERATOR && tt != LOGICALOPERATOR) {
                    obj = getValue(t);
                } else {
                    obj = t.getToken();
                }
                sb.append(obj);
            } else {
                TokenType tt = t.getTokenType();
                if (tt != UNARYOPERATOR) {
                    sb.append(getValue(t));
                } else if (!t.getToken().equals("+")) {
                    throw new SyntaxError("Não é possível realizar operações aritméticas com strings!");
                }
            }
        }
        try {
            if (!stringConcat) {
                return scriptEngine.eval(sb.toString());
            } else {
                return sb.toString();
            }
        } catch (ScriptException e) {
            throw new SyntaxError("Não foi possível executar a expressão! (Expressão: '" + sb.toString() + "')");
        }
    }

    protected boolean isValidExpression(List<Token> tokens) {
        return isValidExpression(tokens, true);
    }

    private boolean isValidExpression(List<Token> tokens, boolean zeroSize) {
        if (!zeroSize && tokens.size() == 0) return false;
        for (Token token : tokens) {
            TokenType tt = token.getTokenType();
            if (tt != IDENTIFIER && tt != STRINGLITERAL && tt != HEXLITERAL && tt != NUMBERLITERAL && tt != BOOLEANLITERAL
                    && tt != UNARYOPERATOR && tt != RELATIONALOPERATOR && tt != LOGICALOPERATOR && tt != OPENPARENTHESIS
                    && tt != CLOSEPARENTHESIS) {
                return false;
            }
        }
        return true;
    }

    protected boolean hasInRange(List<Token> tokens, TokenType type, int start, int end) {
        return getIndex(tokens, type, start, end) != -1;
    }

    private int getIndex(List<Token> tokens, TokenType type, int start, int end) {
        for (int i = start; i < end; i++) {
            if (!typeDiffers(tokens.get(i), type)) {
                return i;
            }
        }
        return -1;
    }

    protected int getIndex(List<Token> tokens, TokenType type) {
        return getIndex(tokens, type, 0, tokens.size());
    }
}
