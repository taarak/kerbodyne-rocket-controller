package com.kerbodyne.kerbolang.structure.keywords;

import com.kerbodyne.kerbolang.structure.KerbolangElement;
import com.kerbodyne.kerbolang.structure.Keyword;
import com.kerbodyne.kerbolang.structure.Variable;
import com.kerbodyne.kerbolang.tokenizer.Token;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.kerbodyne.kerbolang.tokenizer.TokenType.ASSIGNMENTOPERATOR;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.BOOLEANLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.CONSTKEYWORD;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.DEFKEYWORD;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.HEXLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.IDENTIFIER;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.LOGICALOPERATOR;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.NUMBERLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.RELATIONALOPERATOR;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.STRINGLITERAL;
import static com.kerbodyne.kerbolang.tokenizer.TokenType.UNARYOPERATOR;

@KerbolangElement
public class VariableDefinition extends Keyword {
    @Override
    public boolean matches(List<Token> tokens) {
        if (tokens.size() != 2 && !(tokens.size() >= 4)) {
            return false;
        }

        if ((typeDiffers(tokens.get(0), DEFKEYWORD) && typeDiffers(tokens.get(0), CONSTKEYWORD)) ||
                typeDiffers(tokens.get(1), IDENTIFIER)) {
            return false;
        }

        if (tokens.size() > 2) {
            if (typeDiffers(tokens.get(2), ASSIGNMENTOPERATOR)) {
                return false;
            }
            for (Token token : new CopyOnWriteArrayList<>(tokens.subList(3, tokens.size()))) {
                if (typeDiffers(token, STRINGLITERAL) && typeDiffers(token, HEXLITERAL) &&
                        typeDiffers(token, NUMBERLITERAL) && typeDiffers(token, BOOLEANLITERAL) &&
                        typeDiffers(token, IDENTIFIER) && typeDiffers(token, UNARYOPERATOR) &&
                        typeDiffers(token, RELATIONALOPERATOR) && typeDiffers(token, LOGICALOPERATOR)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void run(List<Token> tokens, int lineNumber) {
        Object value = null;
        if (tokens.size() >= 4) {
            value = evaluate(new CopyOnWriteArrayList<>(tokens.subList(3, tokens.size())));
        }
        new Variable(tokens.get(1).getToken(), value, tokens.get(0).getTokenType() == CONSTKEYWORD);
    }
}
