package com.kerbodyne.kerbolang;

public class SyntaxError extends Error {
    private int line;

    public SyntaxError(String message, int line) {
        super(message.replace("{l}", String.valueOf(line)));
        this.line = line;
    }

    public SyntaxError(String message) {
        super(message);
        this.line = 0;
    }

    public int getLine() {
        return line;
    }
}
