package com.kerbodyne.kerbolang;

public class KerbolangInternalError extends RuntimeException {
    public KerbolangInternalError(String message) {
        super(message);
    }

    public KerbolangInternalError(Throwable throwable) {
        super(throwable);
    }

    public KerbolangInternalError(String message, Throwable throwable) {
        super(message, throwable);
    }
}
