package com.kerbodyne.kerbolang.tokenizer;

public enum TokenType {
    DEFKEYWORD("def"),
    CONSTKEYWORD("const"),
    PARALELOKEYWORD("paralelo"),
    QUANDOKEYWORD("quando"),
    ENQUANTOKEYWORD("enquanto"),
    PARAKEYWORD("para"),
    SENAOKEYWORD("senao"),
    SEKEYWORD("se"),
    ENTAOKEYWORD("entao"),
    FIMKEYWORD("fim"),

    STRINGLITERAL("\"[^\"]*\""), HEXLITERAL("0x[a-fA-F\\d]+"), NUMBERLITERAL("-?\\d+(\\.\\d+)?"),
    BOOLEANLITERAL("ligado|desligado"),

    EOS("\n"), WHITESPACE(" "),

    INCREMENT("\\+\\+"), DECREMENT("--"), UNARYASSIGNMENTOPERATOR("[+/*-]="),
    UNARYOPERATOR("[+/*-]"), RELATIONALOPERATOR("==|!=|>=|<=|>|<"), LOGICALOPERATOR("&&|\\|\\|"),
    ASSIGNMENTOPERATOR("="),

    OPENPARENTHESIS("\\("), CLOSEPARENTHESIS("\\)"), COMMA(","),

    IDENTIFIER("[a-zA-Z][a-zA-Z0-9]*"),

    COMMENT(";.*");

    public final String regex;

    TokenType(String regex) {
        this.regex = regex;
    }
}
