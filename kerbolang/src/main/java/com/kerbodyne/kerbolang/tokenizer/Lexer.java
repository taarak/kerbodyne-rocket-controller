package com.kerbodyne.kerbolang.tokenizer;

import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
    public static List<Token> lex(String code) {
        List<Token> tokens = new CopyOnWriteArrayList<>();

        StringJoiner tokenPatternsJoiner = new StringJoiner("|");
        TokenType[] tokenTypes = TokenType.values();
        for (TokenType tokenType : tokenTypes) {
            tokenPatternsJoiner.add(String.format("(?<%s>%s)", tokenType.name(), tokenType.regex));
        }
        Pattern tokenPatterns = Pattern.compile(tokenPatternsJoiner.toString());

        Matcher matcher = tokenPatterns.matcher(code);
        while (matcher.find()) {
            for (TokenType tokenType : tokenTypes) {
                String group = matcher.group(tokenType.name());
                if (group != null && tokenType != TokenType.WHITESPACE && tokenType != TokenType.COMMENT) {
                    tokens.add(new Token(tokenType, group));
                }
            }
        }

        return tokens;
    }
}
