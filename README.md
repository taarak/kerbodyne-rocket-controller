# Kerbodyne Rocket Controller

O Kerbodyne Rocket Controller (ou KRC) é um software para controle de
foguetes, mas por ter uma forma flexível de se comunicar com tais, não é
limitado a eles.

A ideia começou com um projeto na UFSC, onde iremos fazer foguetes e, ao
ver a precariedade das técnicas utilizadas para o controle de tais,
decidi desenvolver um programa que pudesse controlá-los.

## Programação de Tarefas

Para programar Tarefas, que são a forma do KRC de enviar e receber
comandos de um foguete, é necessário o uso da linguagem de programação
de domínio específico chamada Kerbolang.

Tal linguagem é extremamente
simples, e pode basicamente esperar por eventos, chamar outras Tarefas,
ler dados enviados pelo foguete e acionar eventos no mesmo.

### Estruturas de Dados na Kerbolang

Por ser uma linguagem de domínio específico, não são necessário muitos
tipos de dados, nem orientação a objetos. Justamente por isso, existem
somente quatro tipos de dados, onde um deles é convertido para outro ao
ser definido para uma variável ou constante:

* Literais String
* Literais numéricas
* Literais hexadecimais
* Literais booleanas

#### Literais String

Uma literal String é definida quando qualquer caractere está contido
dentro de duas aspas duplas. Por exemplo:

```
"Essa é uma literal String"
```

Este é um exemplo de literal String.

#### Literal Numérica

Uma literal numérica é qualquer valor numérico de base 10, sendo ele de
ponto inteiro ou flutuante.

#### Literal Hexadecimal

Uma literal hexadecimal funciona da mesma forma como uma literal
Numérica, mas tem como prefixo `0x`, e então o seu valor
hexadecimal. Por exemplo:

```
0xFF
```

Esse exemplo irá resultar em um valor numérico de 255.

#### Literal Booleana

Uma literal booleana é um valor binário, que representa ligado ou
desligado. Justamente estas são as palavras reservadas que indicam tais
estados.

### Variáveis e Constantes

É possível declarar uma variável na Kerbolang utilizando a palavra
reservada `def`, e é possível definir constantes com a
palavra reservada `const`.

Variáveis são identificadores para valores que podem ser alterados,
enquanto que constantes são identificadores para valores que não podem
ser modificados no decorrer da tarefa.

Por exemplo:

```
def variavel = "Literal String"
const constante = 0xA0B1C2
```

### Comentários

Comentários são peças de código que não são executados pelo
interpretador Kerbolang, e portanto é possível escrever qualquer texto
em um comentário sem alterar o funcionamento do pedaço de código onde
está contido.

Para criar um comentário na Kerbolang, é necessário utilizar um `;` e
depois começar a escrever seu comentário. Tudo após o `;` será
completamente ignorado pelo interpretador.

Por exemplo:

```
def variavel = "exemplo" ; Após o ponto e vírgula, nada mais será executado!
```
