package com.kerbodyne.rocketcontroller;

import java.util.ArrayList;
import java.util.List;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.TokenMap;
import org.fife.ui.rsyntaxtextarea.modes.UnixShellTokenMaker;

public class KerbolangTokenMaker extends UnixShellTokenMaker {
    public static final List<String> keywords = new ArrayList<>();
    public static final List<String> funcoes = new ArrayList<>();
    public static final List<String> datatypes = new ArrayList<>();

    static {
        // O keyword 'retornar' é bem mais complicado de se fazer com o modelo que estou usando
        // keywords.add("retornar");

        keywords.add("paralelo");
        keywords.add("se");
        keywords.add("senao");
        keywords.add("entao");
        keywords.add("fim");
        keywords.add("enquanto");
        keywords.add("para");
        keywords.add("quando");
        keywords.add("ligado");
        keywords.add("desligado");

        funcoes.add("acione");
        funcoes.add("espere");
        funcoes.add("leia");
        funcoes.add("escreva");
        funcoes.add("escrevaln");
        funcoes.add("reiniciarArduino");

        datatypes.add("def");
        datatypes.add("const");
    }

    @Override
    public TokenMap getWordsToHighlight() {
        TokenMap tokenMap = new TokenMap();

        keywords.forEach((keyword) -> tokenMap.put(keyword, Token.RESERVED_WORD));
        funcoes.forEach((function) -> tokenMap.put(function, Token.FUNCTION));
        datatypes.forEach((datatype) -> tokenMap.put(datatype, Token.DATA_TYPE));
        tokenMap.put(";", Token.COMMENT_EOL);

        /* Keywords que não devem ser auto-completadas */

        tokenMap.put(",", Token.SEPARATOR);
        tokenMap.put("(", Token.SEPARATOR);
        tokenMap.put(")", Token.SEPARATOR);

        tokenMap.put("+", Token.OPERATOR);
        tokenMap.put("-", Token.OPERATOR);
        tokenMap.put("*", Token.OPERATOR);
        tokenMap.put("/", Token.OPERATOR);
        tokenMap.put("=", Token.OPERATOR);
        tokenMap.put("==", Token.OPERATOR);
        tokenMap.put(">=", Token.OPERATOR);
        tokenMap.put("<=", Token.OPERATOR);
        tokenMap.put("!=", Token.OPERATOR);

        return tokenMap;
    }
}
