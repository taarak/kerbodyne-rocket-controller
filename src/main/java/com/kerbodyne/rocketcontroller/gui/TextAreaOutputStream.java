package com.kerbodyne.rocketcontroller.gui;

import java.io.OutputStream;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class TextAreaOutputStream extends OutputStream {
    private JTextArea textArea;
    private StringBuilder sb = new StringBuilder();

    public TextAreaOutputStream(JTextArea textArea) {
        this.textArea = textArea;
    }

    @Override
    public void write(int b) {
        if (b == '\r') {
            return; // TODO: Implementar sistema para voltar ao início da linha ao invés de ignorar
        } else if (b == '\n') {
            String text = sb.toString() + "\n";
            SwingUtilities.invokeLater(() -> textArea.append(text));
            sb.setLength(0);

            return;
        }

        sb.append((char) b);
    }

    @Override
    public void flush() {
        String text = sb.toString();
        SwingUtilities.invokeLater(() -> textArea.append(text));
        sb.setLength(0);
    }
}
