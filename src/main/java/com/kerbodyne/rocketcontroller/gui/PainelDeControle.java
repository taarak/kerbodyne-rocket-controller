package com.kerbodyne.rocketcontroller.gui;

import com.kerbodyne.kerbolang.KerbolangInterpreter;
import com.kerbodyne.kerbolang.comms.Communotron;
import com.kerbodyne.rocketcontroller.KerbodyneRocketController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import static com.kerbodyne.rocketcontroller.KerbodyneRocketController.lerIcone;

public final class PainelDeControle extends PainelGui {
    private static final Border LINE_BORDER = BorderFactory.createLineBorder(Color.DARK_GRAY);
    private static final Dimension DIMENSION = new Dimension(1024, 768);
    private JPanel tarefas; // Painel esquerdo, com as tarefas que podem ser executadas, agendadas ou sofrerem
    // operações CRUD
    private DefaultListModel<String> listaTarefasModel;
    private JList<String> listaTarefas;
    private JButton executarTarefa, adicionarTarefa, editarTarefa, excluirTarefa;
    private JPanel infoFoguetePanel; // Painel central, mostra as informações que vão sendo recebidas sobre o foguete
    private JTextPane infoFoguete; // Label que mostra as informações recebidas sobre o foguete

    private JPanel acionarRapido; // Painel da direita, mostra ações que podem ser executadas instantâneamente.
    // Obviamente deve ser completamente inútil depois da fase de testes básicos, para garantir a segurança, mas como
    // a Kerbolang está muito muito longe de ser terminada, é o que temos.
    private JButton arAcionar;

    private JTextArea console;

    private KerbolangInterpreter kerbolangInterpreter;

    private static boolean resetKerbolangConsole = true;

    public static Border criarBordaTitulada(String titulo) {
        return BorderFactory.createTitledBorder(LINE_BORDER, titulo, TitledBorder.LEFT,
                TitledBorder.DEFAULT_POSITION);
    }

    public static boolean isResetKerbolangConsole() {
        return resetKerbolangConsole;
    }

    private String getSelectedScheduleFileName() {
        return listaTarefas.getSelectedValue() + ".kbl";
    }

    public static void setResetKerbolangConsole(boolean resetKerbolangConsole) {
        PainelDeControle.resetKerbolangConsole = resetKerbolangConsole;
    }

    @Override
    protected void inicializar() {
        kerbolangInterpreter = new KerbolangInterpreter();

        /* TAREFAS INÍCIO */

        tarefas = new JPanel(new BorderLayout());
        tarefas.setBorder(criarBordaTitulada("Tarefas"));

        JPanel scheduleButtonPanel = new JPanel();
        try {
            executarTarefa = new JButton(lerIcone("textures/run.png"));
            executarTarefa.setToolTipText("Executar a Tarefa selecionada");
            executarTarefa.addActionListener(e -> {
                KerbolangInterpreter.clearVariables();

                try {
                    Path tarefaPath = Paths.get(KerbodyneRocketController.TAREFAS_FOLDER_NAME,
                            getSelectedScheduleFileName());
                    String codigoKerbolang = Files.readString(tarefaPath);
                    if (resetKerbolangConsole) console.setText("");
                    new Thread(() -> kerbolangInterpreter.interpret(codigoKerbolang)).start();
                } catch (IOException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(this,
                            "Não foi possível executar o código fonte Kerbolang, pois não foi possível " +
                                    "ler o arquivo o contendo!", "Erro ao ler o " +
                            "arquivo Kerbolang!", JOptionPane.ERROR_MESSAGE);
                }
            });

            adicionarTarefa = new JButton(lerIcone("textures/create.png"));
            adicionarTarefa.setToolTipText("Criar uma nova Tarefa");
            adicionarTarefa.addActionListener(e -> new EditorDeTarefas().setVisible(true));

            editarTarefa = new JButton(lerIcone("textures/edit.png"));
            editarTarefa.setToolTipText("Editar a Tarefa selecionada");
            editarTarefa.addActionListener(e -> {
                if (!listaTarefas.isSelectionEmpty()) {
                    new EditorDeTarefas(Paths.get(KerbodyneRocketController.TAREFAS_FOLDER_NAME,
                            getSelectedScheduleFileName())).setVisible(true);
                }
            });

            excluirTarefa = new JButton(lerIcone("textures/remove.png"));
            excluirTarefa.setToolTipText("Excluir a Tarefa Selecionada");
            excluirTarefa.addActionListener(e -> {
                if (listaTarefas.isSelectionEmpty()) {
                    return; // Nesse caso é mais simples retornar do que envolver tudo em um if
                }

                int option = JOptionPane.showConfirmDialog(this,
                        "Você tem certeza que quer excluir essa tarefa?", "Deseja excluir?",
                        JOptionPane.YES_NO_CANCEL_OPTION);
                if (option == JOptionPane.YES_OPTION) {
                    try {
                        Files.delete(Paths.get("tarefas", getSelectedScheduleFileName()));
                        KerbodyneRocketController.updateSchedulesList(listaTarefasModel);
                    } catch (IOException ex) {
                        throw new UncheckedIOException(ex);
                    }
                }
            });
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this,
                    "Erro ao fazer operação no sistema de arquivos!",
                    "Erro!", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            System.exit(2);
        }
        scheduleButtonPanel.add(executarTarefa);
        scheduleButtonPanel.add(adicionarTarefa);
        scheduleButtonPanel.add(editarTarefa);
        scheduleButtonPanel.add(excluirTarefa);

        listaTarefasModel = new DefaultListModel<>();
        listaTarefas = new JList<>(listaTarefasModel);
        listaTarefas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        try {
            KerbodyneRocketController.updateSchedulesList(listaTarefasModel);
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Erro lendo as tarefas!", "Erro!",
                    JOptionPane.ERROR_MESSAGE);
        }
        KerbodyneRocketController.setScheduleListModel(listaTarefasModel);

        tarefas.add(listaTarefas, BorderLayout.CENTER);
        tarefas.add(scheduleButtonPanel, BorderLayout.SOUTH);

        /* TAREFAS FIM */
        /* INFORMAÇÕES DO FOGUETE INÍCIO */

        infoFoguetePanel = new JPanel(new BorderLayout());
        infoFoguetePanel.setBorder(criarBordaTitulada("Estatísticas & Informações"));

        infoFoguete = new JTextPane();
        infoFoguete.setEditable(false);
        infoFoguetePanel.add(new JScrollPane(infoFoguete), BorderLayout.CENTER);

        /* INFORMAÇÕES DO FOGUETE FIM */
        /* ACIONAR RÁPIDO INÍCIO */

        acionarRapido = new JPanel();
        acionarRapido.setLayout(new BoxLayout(acionarRapido, BoxLayout.Y_AXIS));
        acionarRapido.setBorder(criarBordaTitulada("Acionar Rápido"));

        JTextField arLocalizacao = new JFormattedTextField();
        arLocalizacao.setMaximumSize(new Dimension(arLocalizacao.getMaximumSize().width,
                arLocalizacao.getPreferredSize().height));
        arLocalizacao.setAlignmentX(Component.LEFT_ALIGNMENT);
        JLabel arll = new JLabel("Localização");
        arll.setAlignmentX(Component.LEFT_ALIGNMENT);
        acionarRapido.add(arll);
        acionarRapido.add(arLocalizacao);

        acionarRapido.add(Box.createVerticalStrut(5));

        JComboBox<String> arEstado = new JComboBox<>(new String[]{"Ligado", "Desligado"});
        arEstado.setMaximumSize(new Dimension(arEstado.getMaximumSize().width, arEstado.getPreferredSize().height));
        arEstado.setAlignmentX(Component.LEFT_ALIGNMENT);
        JLabel arel = new JLabel("Estado");
        arel.setAlignmentX(Component.LEFT_ALIGNMENT);
        acionarRapido.add(arel);
        acionarRapido.add(arEstado);

        acionarRapido.add(Box.createVerticalStrut(10));

        arAcionar = new JButton("Acionar");
        arAcionar.addActionListener(ae -> {
            Communotron communotron = KerbolangInterpreter.getCommunotron();
            communotron.setPinState(Integer.parseInt(arLocalizacao.getText()),
                    // O estado só pode ser 'Ligado' e 'Desligado', então se não for 'Ligado' só pode ser 'Desligado'
                    arEstado.getSelectedItem().equals("Ligado"));
        });
        arAcionar.setAlignmentX(Component.LEFT_ALIGNMENT);
        acionarRapido.add(arAcionar);

        /* ACIONAR RÁPIDO FIM */
        /* CONSOLE KERBOLANG INÍCIO */

        JPanel consolePanel = new JPanel(new BorderLayout());
        console = new JTextArea();
        console.setEditable(false);
        console.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 13));
        consolePanel.add(new JScrollPane(console), BorderLayout.CENTER);

        JToolBar consoleToolbar = new JToolBar(JToolBar.VERTICAL);
        consoleToolbar.setFloatable(false);
        try {
            JButton novoConsole = new JButton(lerIcone("textures/new.png"));
            novoConsole.setToolTipText("Limpa o console Kerbolang");
            novoConsole.addActionListener(ae -> console.setText(""));
            consoleToolbar.add(novoConsole);

            JButton pararKerbolang = new JButton(lerIcone("textures/stop.png"));
            pararKerbolang.setMnemonic(KeyEvent.VK_S);
            pararKerbolang.setToolTipText("Para a execução do interpretador Kerbolang");
            pararKerbolang.addActionListener(ae -> KerbolangInterpreter.cancel());
            consoleToolbar.add(pararKerbolang);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this,
                    "Erro ao fazer operação no sistema de arquivos!",
                    "Erro!", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            System.exit(2);
        }
        consolePanel.add(consoleToolbar, BorderLayout.WEST);

        TextAreaOutputStream textAreaOutputStream = new TextAreaOutputStream(console);
        KerbolangInterpreter.setPrintStream(new PrintStream(textAreaOutputStream));

        /* CONSOLE KERBOLANG FIM */
        /* JMENUBAR INÍCIO */

        JMenuBar menuBar = new JMenuBar();
        JMenu menuArquivos = new JMenu("Arquivo");
        JMenu menuKerbolang = new JMenu("Kerbolang");
        JMenu menuArduino = new JMenu("Arduino");
        try {
            /* ARQUIVOS */
            JMenuItem importarTarefa = new JMenuItem("Importar",
                    lerIcone("textures/import.png"));
            importarTarefa.setAccelerator(KeyStroke.getKeyStroke("ctrl I"));
            importarTarefa.addActionListener(ae -> KerbodyneRocketController.importSchedule());
            menuArquivos.add(importarTarefa);

            JMenuItem atualizarListaDeTarefas = new JMenuItem("Atualizar Lista de Tarefas",
                    lerIcone("textures/refresh.png"));
            atualizarListaDeTarefas.setAccelerator(KeyStroke.getKeyStroke("F5"));
            atualizarListaDeTarefas.addActionListener(ae -> {
                try {
                    KerbodyneRocketController.updateSchedulesList(listaTarefasModel);
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(this,
                            "Erro ao atualizar a lista de tarefas!",
                            "Erro!", JOptionPane.ERROR_MESSAGE);
                    e.printStackTrace();
                }
            });
            menuArquivos.add(atualizarListaDeTarefas);

            menuArquivos.addSeparator();

            JMenuItem preferencias = new JMenuItem("Preferências",
                    lerIcone("textures/preferences.png"));
            preferencias.setAccelerator(KeyStroke.getKeyStroke("ctrl P"));
            preferencias.addActionListener(ae -> new Preferencias().setVisible(true));
            menuArquivos.add(preferencias);

            /* KERBOLANG */

            JMenuItem executarTarefaMenu = new JMenuItem("Executar");
            executarTarefaMenu.setAccelerator(KeyStroke.getKeyStroke("ctrl shift R"));
            executarTarefaMenu.addActionListener(ae -> executarTarefa.doClick());
            menuKerbolang.add(executarTarefaMenu);

            JMenuItem adicionarTarefaMenu = new JMenuItem("Adicionar");
            adicionarTarefaMenu.setAccelerator(KeyStroke.getKeyStroke("ctrl shift A"));
            adicionarTarefaMenu.addActionListener(ae -> adicionarTarefa.doClick());
            menuKerbolang.add(adicionarTarefaMenu);

            JMenuItem editarTarefaMenu = new JMenuItem("Editar");
            editarTarefaMenu.setAccelerator(KeyStroke.getKeyStroke("ctrl shift E"));
            editarTarefaMenu.addActionListener(ae -> editarTarefa.doClick());
            menuKerbolang.add(editarTarefaMenu);

            JMenuItem excluirTarefaMenu = new JMenuItem("Excluir");
            excluirTarefaMenu.setAccelerator(KeyStroke.getKeyStroke("ctrl alt E"));
            excluirTarefaMenu.addActionListener(ae -> excluirTarefa.doClick());
            menuKerbolang.add(excluirTarefaMenu);

            /* ARDUINO */

            JMenuItem reconectarArduino = new JMenuItem("Reconectar com o Arduino",
                    lerIcone("textures/find.png"));
            reconectarArduino.addActionListener(ae -> KerbodyneRocketController.reconnectArduino());
            menuArduino.add(reconectarArduino);
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Não foi possível carregar os arquivos de imagem!",
                    "Erro!", JOptionPane.ERROR_MESSAGE);
            System.exit(2);
        }
        menuBar.add(menuArquivos);
        menuBar.add(menuKerbolang);
        menuBar.add(menuArduino);

        setJMenuBar(menuBar);

        /* JMENUBAR FIM */

        JSplitPane centerPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, infoFoguetePanel, acionarRapido);
        centerPanel.setResizeWeight(0.9d);

        JSplitPane leftCenterSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, tarefas, centerPanel);
        leftCenterSplitPane.setDividerLocation(tarefas.getPreferredSize().getWidth() / DIMENSION.getWidth());

        JSplitPane mainSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, leftCenterSplitPane, consolePanel);
        mainSplitPane.setResizeWeight(0.7d);

        JPanel contentPane = new JPanel(new BorderLayout());
        contentPane.add(mainSplitPane, BorderLayout.CENTER);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                int option = JOptionPane.showConfirmDialog(PainelDeControle.this,
                        "Você quer realmente fechar o Kerbodyne Rocket Controller?");
                if (option == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });

        setTitle("Controle da Missão - Kerbodyne Rocket Controller");
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setSize(DIMENSION);
        setContentPane(contentPane);
        setIconImage(KerbodyneRocketController.kerbodyneImage);
        setLocationRelativeTo(null);
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
    }
}
