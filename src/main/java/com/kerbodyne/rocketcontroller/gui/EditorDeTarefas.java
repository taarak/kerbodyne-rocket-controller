package com.kerbodyne.rocketcontroller.gui;

import com.kerbodyne.rocketcontroller.KerbodyneRocketController;
import com.kerbodyne.rocketcontroller.KerbolangTokenMaker;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;

public final class EditorDeTarefas extends PainelGui {
    private static final Dimension DIMENSION = new Dimension(800, 600);
    private RSyntaxTextArea codeEditor;
    private JMenuBar menuBar;
    private Path codeFile;

    EditorDeTarefas() {
        super();
    }

    EditorDeTarefas(Path codeFile) {
        super(); // Vai chamar #inicializar()
        this.codeFile = codeFile;
        try {
            codeEditor.setText(Files.readString(codeFile));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    protected void inicializar() {
        JPanel contentPane = new JPanel(new BorderLayout());

        codeEditor = new RSyntaxTextArea();
        codeEditor.setSyntaxEditingStyle("text/kerbolang");
        codeEditor.setCodeFoldingEnabled(true);
        codeEditor.setMarkOccurrences(true);

        AutoCompletion autoCompletion = new AutoCompletion(criarCompletionProvider());
        autoCompletion.install(codeEditor);

        RTextScrollPane codeEditorScrollPane = new RTextScrollPane(codeEditor);
        codeEditorScrollPane.setLineNumbersEnabled(true);
        codeEditorScrollPane.setFoldIndicatorEnabled(true);
        codeEditorScrollPane.setIconRowHeaderEnabled(true); // For the fanciness

        contentPane.add(codeEditorScrollPane, BorderLayout.CENTER);

        menuBar = new JMenuBar();
        JMenu file = new JMenu("Arquivo");
        try {
            JMenuItem save = new JMenuItem("Salvar", KerbodyneRocketController.lerIcone("textures/save.png"));
            save.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
            save.addActionListener((ae) -> KerbodyneRocketController.save(codeFile, codeEditor.getText()));
            file.add(save);

            JMenuItem saveAs = new JMenuItem("Salvar Como",
                    KerbodyneRocketController.lerIcone("textures/save-as.png"));
            saveAs.setAccelerator(KeyStroke.getKeyStroke("ctrl shift S"));
            saveAs.addActionListener((ae) -> KerbodyneRocketController.saveAs(codeFile, codeEditor.getText()));
            file.add(saveAs);
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Não foi possível carregar os arquivos de imagem!",
                    "Erro!", JOptionPane.ERROR_MESSAGE);
            System.exit(2);
        }
        menuBar.add(file);

        setTitle("Editor de Tarefas - Kerbodyne Rocket Controller");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(DIMENSION);
        setContentPane(contentPane);
        setJMenuBar(menuBar);
        setIconImage(KerbodyneRocketController.kerbodyneImage);
    }

    private CompletionProvider criarCompletionProvider() {
        DefaultCompletionProvider provider = new DefaultCompletionProvider();

        KerbolangTokenMaker.keywords.forEach((keyword) -> provider.addCompletion(new BasicCompletion(provider, keyword)));
        KerbolangTokenMaker.funcoes.forEach((function) -> provider.addCompletion(new BasicCompletion(provider, function)));
        KerbolangTokenMaker.datatypes.forEach((datatype) -> provider.addCompletion(new BasicCompletion(provider, datatype)));

        return provider;
    }
}
