package com.kerbodyne.rocketcontroller.gui;

import com.kerbodyne.kerbolang.KerbolangInterpreter;
import com.kerbodyne.rocketcontroller.KerbodyneRocketController;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

public final class Preferencias extends PainelGui {
    @Override
    protected void inicializar() {
        JPanel contentPane = new JPanel(new BorderLayout());

        JPanel kerbolang = new JPanel();
        kerbolang.setBorder(PainelDeControle.criarBordaTitulada("Kerbolang"));
        kerbolang.setLayout(new BoxLayout(kerbolang, BoxLayout.Y_AXIS));

        JCheckBox kerbolangTrace = new JCheckBox("Ativar 'trace' ao executar código Kerbolang");
        kerbolangTrace.setSelected(KerbolangInterpreter.isTrace());
        kerbolangTrace.addActionListener(ae -> KerbolangInterpreter.setTrace(kerbolangTrace.isSelected()));
        kerbolang.add(kerbolangTrace);

        JCheckBox resetKerbolangConsole = new JCheckBox("Apagar Console Kerbolang antes de cada execução");
        resetKerbolangConsole.setSelected(PainelDeControle.isResetKerbolangConsole());
        resetKerbolangConsole.addActionListener(ae ->
                PainelDeControle.setResetKerbolangConsole(resetKerbolangConsole.isSelected()));
        kerbolang.add(resetKerbolangConsole);

        contentPane.add(kerbolang, BorderLayout.WEST);

        setContentPane(contentPane);
        setTitle("Preferências - Kerbodyne Rocket Controller");
        setIconImage(KerbodyneRocketController.kerbodyneImage);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
    }
}
