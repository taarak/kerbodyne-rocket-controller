package com.kerbodyne.rocketcontroller.gui;

import javax.swing.JFrame;

public abstract class PainelGui extends JFrame {
    PainelGui() {
        inicializar();
    }

    protected abstract void inicializar();
}
