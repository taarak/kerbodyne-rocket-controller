package com.kerbodyne.rocketcontroller;

import com.kerbodyne.kerbolang.KerbolangInterpreter;
import com.kerbodyne.kerbolang.comms.ArdulinkCommunotron;
import com.kerbodyne.kerbolang.comms.Communotron;
import com.kerbodyne.rocketcontroller.gui.PainelDeControle;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.commons.io.FilenameUtils;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;

public class KerbodyneRocketController {
    public static final String TAREFAS_FOLDER_NAME = "tarefas";
    private static final KerbodyneRocketController krc;
    private static final ClassLoader classLoader;
    public static Image kerbodyneImage;
    private static DefaultListModel<String> scheduleListModel;

    static {
        krc = new KerbodyneRocketController();
        classLoader = krc.getClass().getClassLoader();
    }

    public static void main(String[] args) throws IOException {
        System.err.println("Uma NullPointerExceptions podem ser lançadas quando o usuário pressiona 'Cancelar' no " +
                "JFileChooser, mas, como elas podem ser ignoradas, decidi deixar que fossem lançadas");

        // Lê todas as classes que tem a anotação @KerbolangElement e as adiciona para a lista de keywords registradas.
        KerbolangInterpreter.startup();

        reconnectArduino();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (KerbolangInterpreter.getCommunotron() != null) {
                try {
                    KerbolangInterpreter.getCommunotron().close();
                } catch (IOException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Não foi possível cortar corretamente " +
                            "a conexão com o Arduino!", "Erro!", JOptionPane.ERROR_MESSAGE);
                }
            }
        }));

        kerbodyneImage = lerIcone("textures/kerbodyne-logo.png").getImage();

        AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
        atmf.putMapping("text/kerbolang", "com.kerbodyne.rocketcontroller.KerbolangTokenMaker");

        File tarefasDir = new File(TAREFAS_FOLDER_NAME + "/");
        if (!tarefasDir.exists()) {
            boolean sucesso = tarefasDir.mkdirs();
            if (!sucesso) {
                System.err.println("Não foi possível criar o diretório de tarefas!");
                JOptionPane.showMessageDialog(null, "Não foi possível criar o diretório de " +
                        "tarefas! O programa será terminado!", "Erro!", JOptionPane.ERROR_MESSAGE);
                System.exit(-10);
            }
        }

        SwingUtilities.invokeLater(() -> {
            System.setProperty("awt.useSystemAAFontSettings", "on");
            try {
                new PainelDeControle().setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Um erro fatal ocorreu e é necessário " +
                                "fechar o programa. É melhor correr e tentar ativar o paraquedas se algum foguete " +
                                "estava em voo e sendo controlado pelo Kerbodyne Rocket Controller.",
                        "Erro Fatal!",
                        JOptionPane.ERROR_MESSAGE);
                System.exit(-11);
            }
        });
    }

    public static ImageIcon lerIcone(String caminho) throws IOException {
        return new ImageIcon(ImageIO.read(Objects.requireNonNull(classLoader.getResource(caminho),
                "Caminho do recurso inválido (ícone)!")));
    }

    public static void setScheduleListModel(DefaultListModel<String> scheduleListModel) {
        KerbodyneRocketController.scheduleListModel = scheduleListModel;
    }

    public static void save(Path path, String str) {
        if (path == null) {
            saveAs(null, str);
            return;
        }

        try {
            Files.writeString(path, str);
            updateSchedulesList(scheduleListModel);
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Não foi possível salvar o arquivo "
                    + path.getFileName() + "! Erro ao escrever no arquivo!", "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static Path saveAs(Path currentPath, String str) {
        Path path;

        File defaultFolder = new File("./" + TAREFAS_FOLDER_NAME + "/");
        if (!defaultFolder.exists()) {
            boolean sucesso = defaultFolder.mkdirs();
            if (!sucesso) {
                throw new UnsuccessfulOperationException("Não foi possível criar os diretórios para salvar o arquivo!");
            }
        }

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(currentPath != null ? currentPath.toFile() : defaultFolder);
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setFileFilter(new FileNameExtensionFilter("Arquivo de Código-Fonte Kerbolang",
                "kbl"));
        fileChooser.setDialogTitle("Escolher Arquivo");
        int state = fileChooser.showSaveDialog(null);
        if (state != JFileChooser.APPROVE_OPTION) return null;
        String fileString = fileChooser.getSelectedFile().getAbsolutePath();
        if (!fileString.endsWith(".kbl")) {
            fileString += ".kbl";
        }
        path = Path.of(fileString);
        save(path, str);

        return path; // Se ocorrer uma exception (que não deve) o valor retornado já será nulo
    }

    public static void importSchedule() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setFileFilter(new FileNameExtensionFilter("Arquivo de Código-Fonte Kerbolang",
                "kbl"));
        fileChooser.setDialogTitle("Escolher Arquivo para Importar");
        int state = fileChooser.showOpenDialog(null);
        if (state != JFileChooser.APPROVE_OPTION) return;
        File[] files = fileChooser.getSelectedFiles();

        try {
            for (File file : files) {
                Path source = Paths.get(file.getAbsolutePath());
                Path target = Paths.get(TAREFAS_FOLDER_NAME, file.getName());
                Files.copy(source, target);
            }
            updateSchedulesList(scheduleListModel);
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Não foi possível importar os arquivos! "
                    + "Erro ao copiar o arquivo!", "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void updateSchedulesList(DefaultListModel<String> listModel) throws IOException {
        listModel.clear();
        Path schedulesPath = Paths.get(TAREFAS_FOLDER_NAME + "/");
        Files.walk(schedulesPath, 1).forEach(path -> {
            if (Files.isRegularFile(path)) {
                listModel.addElement(FilenameUtils.removeExtension(path.getFileName().toString()));
            }
        });
    }

    public static void reconnectArduino() {
        try {
            Communotron ardulinkCommunotron = new ArdulinkCommunotron();
            ardulinkCommunotron.addMessageReceivenListener(System.out::println); // Place holder
            KerbolangInterpreter.setCommunotron(ardulinkCommunotron);
            ardulinkCommunotron.sendMessage("reset");
            System.out.println("Conectado com sucesso ao Arduino!");
        } catch (Exception e) {
            System.err.println("Erro tentando se conectar com uma placa Arduino");
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Não foi possível se conectar com nenhuma " +
                            "placa Arduino! Verifique se há uma placa Arduino conectada ao computador!",
                    "Erro de conexão!", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static class UnsuccessfulOperationException extends RuntimeException {
        public UnsuccessfulOperationException(String message) {
            super(message);
        }
    }
}
